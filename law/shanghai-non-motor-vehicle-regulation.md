## 上海市非机动车管理办法

（2013年10月20日上海市人民政府令第9号公布）

### 第一章总则

　　第一条（目的和依据）

　　为了加强非机动车管理，保障道路交通安全和畅通，保护公民、法人和其他组织的合法权益，根据《中华人民共和国道路交通安全法》、《中华人民共和国产品质量法》、《中华人民共和国道路交通安全法实施条例》等法律、法规，结合本市实际，制定本办法。

　　第二条（适用范围）

　　本市行政区域内非机动车的生产、销售、登记、通行以及相关管理活动，适用本办法。

　　第三条（职责分工）

　　市公安局是本市非机动车管理的行政主管部门。公安机关交通管理部门具体负责非机动车的登记和通行管理。

　　质量技术监督部门负责非机动车生产质量的监督管理。

　　工商行政管理部门负责非机动车销售的监督管理。

　　经济信息化、建设交通、环境保护、城市管理行政执法、残疾人联合会等部门和单位按照各自职责，做好非机动车管理相关工作。

　　区、县人民政府负责组织相关部门做好辖区内非机动车道路停放管理工作。

　　第四条（调控措施）

　　本市根据城市道路交通发展需求和环境保护实际情况，对特定种类的非机动车实行总量调控或者采取淘汰措施。

### 第二章非机动车生产、销售管理

　　第五条（安全技术要求）

　　在本市生产、销售的非机动车应当符合有关国家标准。

　　电动自行车国家标准中的电动机功率、残疾人机动轮椅车国家标准中的汽油机排量等推荐性项目，在本市强制执行。

　　第六条（产品目录）

　　本市对符合国家和本市标准要求的电动自行车、残疾人机动轮椅车实行产品目录管理制度。因不符合国家和本市标准要求而未纳入产品目录的电动自行车、残疾人机动轮椅车，不得在本市销售和登记上牌。

　　电动自行车产品目录由市经济信息化委会同市质量技监局、市工商局、市公安局、市环保局等部门以及相关行业协会编制。残疾人机动轮椅车产品目录由市经济信息化委、市残疾人联合会会同市质量技监局、市工商局、市公安局、市环保局等部门编制。

　　电动自行车产品目录、残疾人机动轮椅车产品目录应当载明生产企业、品牌、型号、定型技术参数等项目，向社会公示安全性能良好的产品，并适时更新。

　　第七条（变更技术参数）

　　已经纳入产品目录的电动自行车、残疾人机动轮椅车的技术参数发生变更的，负责目录编制的部门应当重新进行核定。

　　生产者擅自更改定型技术参数的，其相应产品从产品目录中删除。

　　第八条（销售承诺）

　　电动自行车、残疾人机动轮椅车的销售者，应当在销售场所醒目位置公示现行有效的产品目录，并通过店堂告示、销售凭证中载明等方式，向消费者承诺其销售车辆已纳入产品目录，符合本市登记上牌条件。

　　消费者购买的电动自行车、残疾人机动轮椅车因未纳入产品目录无法在本市登记上牌的，可以依法要求退货。

　　第九条（禁止拼装、加装、改装）

　　禁止单位和个人从事下列行为：

　　（一）拼装非机动车；

　　（二）在非机动车上加装动力装置、座位、高分贝音响或者擅自加装车篷；

　　（三）改变非机动车排气装置的尺寸或者擅自更换动力装置；

　　（四）拆除或者改动非机动车的消音、限速、尾气处理装置；

　　（五）其他更改非机动车定型技术参数、影响非机动车通行安全的拼装、加装、改装行为。

　　禁止销售拼装、加装、改装的非机动车。

　　第十条（举报投诉）

　　对违法生产、销售非机动车的行为，单位或者个人可以向质量技术监督部门、工商行政管理部门举报、投诉。质量技术监督部门、工商行政管理部门接到举报、投诉后，应当依法及时查处。

　　对调查属实的违法行为，质量技术监督部门、工商行政管理部门应当及时将查处情况通报市经济信息化委、市残疾人联合会等相关部门和单位。

　　第十一条（协调配合机制）

　　公安机关、质量技术监督部门、工商行政管理部门建立协调配合的执法机制。

　　公安机关发现违法生产、销售非机动车的，应当及时通知质量技术监督部门、工商行政管理部门，质量技术监督部门、工商行政管理部门应当依法及时查处。对阻碍质量技术监督部门、工商行政管理部门依法执行职务的行为，公安机关应当依法予以制止和处理。

　　第十二条（环保要求）

　　电动自行车所有人应当将电动自行车的废旧电池送交电动自行车生产者、销售者处理，或者送交具有危险废物处置资质的单位集中处置。

　　电动自行车生产者、销售者应当采取以旧换新等方式回收电动自行车的废旧电池，建立回收台账，送交具有危险废物处置资质的单位集中处置。

　　鼓励电动自行车生产者、销售者采取以旧换新等方式回收废旧电动自行车。

　　第十三条（残疾人轮椅车的更新补贴）

　　本市对残疾人机动轮椅车实行更新补贴制度。已经登记上牌的残疾人机动轮椅车送交指定单位回收的，由残疾人联合会按照规定给予补贴。

### 第三章非机动车登记管理

　　第十四条（登记车种）

　　下列非机动车，应当经本市公安机关交通管理部门登记，取得非机动车号牌和行车执照（以下称非机动车牌证）：

　　（一）电动自行车；

　　（二）残疾人机动轮椅车；

　　（三）人力三轮车；

　　（四）市人民政府规定应当登记上牌的其他非机动车。

　　自行车、残疾人手摇轮椅车实行自愿登记，其所有人申请登记上牌的，公安机关交通管理部门应当予以办理。

　　第十五条（申请登记上牌）

　　对本办法第十四条第一款规定的非机动车，其所有人应当自购车之日起15日内，到公安机关交通管理部门申请登记上牌，现场交验车辆并提交下列材料：

　　（一）身份证、户口簿或者单位营业执照等合法有效的非机动车所有人身份证明；

　　（二）购车凭证或者其他非机动车合法来历证明；

　　（三）非机动车整车出厂合格证明；

　　（四）公安机关交通管理部门要求提交的其他材料。

　　残疾人机动轮椅车的所有人申请登记上牌的，应当到常住户口所在地的区、县公安机关交通管理部门办理，除前款规定的材料外，还应当提交本市残疾人联合会出具的相关证明。

　　第十六条（登记上牌的特殊要求）

　　残疾人机动轮椅车仅限于符合条件的下肢残疾人员申请登记上牌，每人可以登记一辆。

　　人力三轮车仅限于市政、环卫等单位因作业需要申请登记上牌。

　　第十七条（登记上牌）

　　对申请登记上牌的非机动车，公安机关交通管理部门应当进行查验。材料齐全、符合规定的，应当当场登记并发放非机动车牌证；不予登记上牌的，应当向申请人书面说明理由。

　　非机动车牌证由公安机关交通管理部门统一监制。

　　第十八条（变更登记）

　　已经登记上牌的非机动车有下列情形之一的，非机动车所有人应当向公安机关交通管理部门交验车辆，申请办理变更登记：

　　（一）更换车身、车架的；

　　（二）因质量原因更换整车的；

　　（三）残疾人机动轮椅车更换符合安全技术要求的动力装置的；

　　（四）残疾人机动轮椅车所有人的常住户口所在地发生变动的。

　　第十九条（转移登记）

　　已经登记上牌的非机动车所有权发生转移的，非机动车的受让人应当向公安机关交通管理部门交验车辆，申请办理转移登记。

　　人力三轮车不予办理转移登记。

　　第二十条（注销登记）

　　已经登记上牌的非机动车被盗、遗失、灭失或者因质量原因退车的，非机动车所有人应当向公安机关交通管理部门申请办理注销登记。

　　第二十一条（牌证换领、补领）

　　非机动车牌证损坏、灭失的，非机动车所有人应当向公安机关交通管理部门交验车辆，申请换领或者补领非机动车牌证。

　　第二十二条（外省市非机动车登记）

　　外省市号牌非机动车需要在本市通行的，非机动车所有人应当按照本办法规定向公安机关交通管理部门申请登记，取得本市非机动车牌证。

　　第二十三条（信息公开和便民措施）

　　公安机关交通管理部门应当将非机动车登记的条件、程序、需提交的材料和申请表示范文本等向社会公布，并采取增设登记办理点、简化办理程序等方式，为市民办理非机动车登记提供便利。

　　第二十四条（宣传教育）

　　公安机关交通管理部门应当结合非机动车登记管理，对非机动车驾驶人进行道路交通安全法律、法规、规章的宣传教育，增强其道路交通安全意识。

　　第二十五条（登记办法）

　　非机动车登记的具体办法，由市公安局另行制定。

### 第四章非机动车通行管理

　　第二十六条（通行车辆）

　　下列非机动车可以上道路行驶：

　　（一）已经登记上牌的电动自行车、残疾人机动轮椅车、人力三轮车；

　　（二）自行车、残疾人手摇轮椅车；

　　（三）市人民政府规定可以通行的其他非机动车。

　　应当登记上牌的新购车辆，驾驶人可以持购车凭证在购车后15日内临时通行。

　　禁止本条第一款、第二款规定以外的其他非机动车上道路行驶。

　　第二十七条（牌证使用）

　　驾驶已经登记上牌的非机动车上道路行驶的，应当随车携带行车执照，并按照规定安装非机动车号牌，保持号牌清晰、完整，不得故意遮挡、污损。

　　禁止伪造、变造或者使用伪造、变造的非机动车牌证。禁止使用其他车辆的非机动车牌证。

　　第二十八条（基本安全要求）

　　驾驶非机动车上道路行驶的，应当保持制动器、夜间反光装置等安全设施性能状况良好。

　　第二十九条（一般通行规定）

　　驾驶非机动车上道路行驶，应当遵守道路交通安全法律、法规关于道路通行的规定和下列规定：

　　（一）在非机动车道内行驶；在没有划设非机动车道的道路上，自行车、电动自行车在车行道右侧边缘线向左1.5米的范围内行驶，残疾人手摇轮椅车、残疾人机动轮椅车、人力三轮车在车行道右侧边缘线向左2.2米的范围内行驶。

　　（二）除法定可以借道行驶的情况外，不得驶入机动车道。

　　（三）不得驶入高速公路、高架道路、越江隧道和越江桥梁等禁止非机动车通行的区域。

　　（四）行经人行横道时，减速行驶，遇行人正在通过人行横道的，停车让行；行经没有交通信号的道路时，遇行人横过道路的，应当避让。

　　（五）转弯前减速慢行，伸手示意，有转向灯的开启转向灯；超越前车时不得妨碍被超越的车辆行驶。

　　（六）不得实施其他影响安全行驶的行为。

　　禁止驾驶拼装、加装、改装的非机动车上道路行驶。

　　第三十条（特别通行规定）

　　驾驶电动自行车、残疾人机动轮椅车上道路行驶，除遵守第二十九条规定外，还应当遵守下列规定：

　　（一）驾驶人年满16周岁；

　　（二）最高时速不得超过15公里；

　　（三）非下肢残疾人员不得驾驶残疾人机动轮椅车。

　　第三十一条（载人规定）

　　自行车、电动自行车载人，应当遵守下列规定：

　　（一）驾驶自行车、电动自行车限载1名12周岁以下的未成年人；

　　（二）驾驶自行车、电动自行车搭载6周岁以下未成年人的，使用固定座椅；

　　（三）16周岁以下的未成年人驾驶自行车不得载人。

　　第三十二条（载物规定）

　　非机动车载物，应当遵守下列规定：

　　（一）自行车、电动自行车、残疾人手摇轮椅车、残疾人机动轮椅车载物，高度从地面起不得超过1.5米，宽度左右各不得超出车把0.15米，长度前端不得超出车轮，后端不得超出车身0.3米；

　　（二）人力三轮车载物，高度从地面起不得超过2米，宽度左右各不得超出车身0.2米，长度不得超出车身1米；

　　（三）非机动车载物应当采取加固措施，防止发生货物散落、飘洒等影响道路通行的情况。

　　第三十三条（道路停放）

　　在道路上停放非机动车，应当使用非机动车道路停放点。

　　各区、县人民政府应当根据非机动车道路停放点设置规范，编制本区、县非机动车道路停放点的设置规划，指定专门管理部门落实非机动车道路停放点的设置工作，并组建专门管理队伍，加强非机动车道路停放点的日常管理。

　　第三十四条（专用停车场地）

　　车站、码头、轨道交通站点等交通集散地以及医院、学校、商场、集贸市场、步行街、影剧院、体育场馆、展览馆等人员流动较多的场所，其管理者应当设置非机动车专用停车场地，并落实专人管理或者委托专业服务机构管理。

　　第三十五条（非机动车保险）

　　本市鼓励非机动车驾驶人投保第三者责任保险、人身伤害保险和财产损失保险。

### 第五章法律责任

　　第三十六条（指引条款）

　　违反本办法规定的行为，《中华人民共和国道路交通安全法》、《中华人民共和国道路交通安全法实施条例》、《上海市道路交通管理条例》等法律、法规有处理规定的，从其规定。

　　第三十七条（对违法生产、销售非机动车行为的处罚）

　　违反本办法第五条第一款规定，生产、销售不符合国家标准的非机动车的，由质量技术监督部门、工商行政管理部门按照《中华人民共和国产品质量法》、《上海市产品质量条例》的规定处理。

　　违反本办法第六条第一款规定，销售未纳入产品目录的电动自行车、残疾人机动轮椅车的，由工商行政管理部门处5000元以上5万元以下罚款。

　　违反本办法第八条第二款规定，拒不退货的，由工商行政管理部门按照《中华人民共和国消费者权益保护法》的规定处理。

　　第三十八条（对拼装、加装、改装非机动车及其销售行为的处罚）

　　违反本办法第九条规定，从事经营性拼装、加装、改装非机动车或者销售拼装、加装、改装非机动车的，由质量技术监督部门、工商行政管理部门按照各自职责处2000元以上2万元以下罚款。

　　第三十九条（对非机动车道路交通安全违法行为的处罚）

　　违反本办法第十四条第一款、第二十六条第三款规定，驾驶无牌无证的非机动车或者禁止通行的非机动车上道路行驶的，由公安机关交通管理部门处50元以上200元以下罚款。

　　违反本办法第二十七条第一款、第二十八条、第二十九条第一款、第三十条、第三十一条、第三十二条、第三十三条第一款规定的，由公安机关交通管理部门处警告或者5元以上50元以下罚款。

　　违反本办法第二十七条第二款规定，伪造、变造或者使用伪造、变造的非机动车牌证的，以及使用其他车辆的非机动车牌证的，由公安机关交通管理部门处50元以上500元以下罚款；情节严重的，处500元以上1000元以下罚款。

　　违反本办法第二十九条第二款规定，驾驶加装动力装置的自行车、人力三轮车上道路行驶的，由公安机关交通管理部门责令改正，处警告或者5元以上50元以下罚款；驾驶其他拼装、加装、改装的非机动车上道路行驶的，由公安机关交通管理部门责令改正，处50元以上200元以下罚款。

　　违反本办法第三十三条第一款规定，非机动车未停放在非机动车道路停放点，影响其他车辆和行人通行且行为人不在现场的，公安机关交通管理部门可以会同城市管理行政执法部门对现场予以清理。

　　第四十条（治安管理处罚和刑事责任）

　　违反本办法规定，构成违反治安管理行为的，由公安机关依法给予治安管理处罚；构成犯罪的，依法追究刑事责任。

　　第四十一条（行政责任）

　　违反本办法规定，相关行政管理部门及其工作人员有下列行为之一的，由所在单位或者上级主管部门依法对直接负责的主管人员和其他直接责任人员给予行政处分；构成犯罪的，依法追究刑事责任：

　　（一）不依法履行非机动车生产、销售监督管理职责，不依法查处违法生产、销售非机动车行为的；

　　（二）不依法履行非机动车登记、通行管理职责，不依法查处非机动车违法通行行为的；

　　（三）无法定依据或者违反法定程序执法的；

　　（四）其他滥用职权、玩忽职守、徇私舞弊的行为。

### 第六章附则

　　第四十二条（过渡期管理措施）

　　对本办法公布前已经购买但因未纳入产品目录不能登记上牌的电动自行车，其所有人在本办法实施之日起6个月内向公安机关交通管理部门申领临时通行凭证的，可以自本办法实施之日起3年内上道路行驶，并遵守有关非机动车通行管理的规定；期满后，不得上道路行驶。

　　前款管理措施的具体办法由市公安局另行制定。

　　第四十三条（实施日期）

　　本办法自2014年3月1日起施行。2001年9月19日上海市人民政府令第108号发布的《上海市非机动车管理办法》同时废止。